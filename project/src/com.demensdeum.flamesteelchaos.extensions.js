function IOExtensions() {
  this.enable = function() {
    Array.last = function() {
      return this[this.length - 1];
    };

    String.lastPathComponent = function() {
      var splitted = this.split("/");
      return splitted.last();
    };
  };
}
