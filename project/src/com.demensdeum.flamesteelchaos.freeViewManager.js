function FreeViewManager(object, speed) {

  this.isJumpEnabled = false;
  this.speed = speed;
  this.objectName = object.name;

  this.rotateByPointer = function(object) {
    object.rotation.x -= pointerXdiff();
    object.rotation.y += pointerYdiff();

    if (object.rotation.y > 89) {
        object.rotation.y = 89;
    }
    else if (object.rotation.y < -89) {
        object.rotation.y = -89;
    }
  };

  this.moveByKeys = function(object) {
    if (isKeyPressed("leftKey")) {
      translateCamera(object, -this.speed, 0, 0);
    }
    else if (isKeyPressed("rightKey")) {
      translateCamera(object, this.speed, 0, 0);
    }
    if (isKeyPressed("upKey")) {
      translateCamera(object, 0, 0, this.speed);
    }
    else if (isKeyPressed("downKey")) {
      translateCamera(object, 0, 0, -this.speed);
    }
    else if (this.isJumpEnabled && isKeyPressed("jumpKey")) {
      translateCamera(object, 0, this.speed, 0);
    }
  };

  this.step = function() {
    var object = getObject(this.objectName);
    this.rotateByPointer(object);
    this.moveByKeys(object);
    updateObject(object);
  };

}
