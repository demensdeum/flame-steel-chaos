if (GLOBAL_CONTEXT === undefined) {
    include("com.demensdeum.flamesteelchaos.includes.js");
    IncludeDependencies();
    GLOBAL_APP_TITLE = "Flame Steel Chaos v0.0.1";
    GLOBAL_CONTEXT = new Context();
    GLOBAL_CONTEXT.initializeGame = function() {
      var controller = new MainController();
      this.switchToController(controller);
    };
}

GLOBAL_CONTEXT.step();
