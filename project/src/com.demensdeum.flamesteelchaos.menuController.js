function MenuController() {

  var states = [
    "default",
    "addObjectModelPicker",
    "freeFly",
    "objectsPicker",
    "objectControl"
  ];

  this.selectedObjectName = null;
  this.state = "default";
  this.objects = new List();
  this.models = new List();
  this.textures = new List();
  this.sounds = new List();
  this.music = new List();

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      this.initialized = true;
      this.models.push("com.demensdeum.flamesteelengine.cube.fsglmodel");
      this.textures.push("com.demensdeum.flamesteelengine.texture");
      var camera = getObject("camera");
      this.freeViewManager = new FreeViewManager(camera, 0.1);
      addText("Flame Steel Chaos 0.0.1");
      var cameraSceneObject = new SceneObject("camera", null);
      this.objects.push(cameraSceneObject);
      this.showMainMenu();
    }
  };

  this.showMainMenu = function() {
    this.state = "default";
    destroyWindow();
    addButton("Objects", "objects");
    addButton("Graphics", "graphics");
    addButton("Sound & Music", "soundNmusic");
    addButton("Scripts", "scripts");
    addButton("Free Fly mode", "freeFly");
    addButton("Save", "save");
    addButton("Load", "load");
  };

  this.showObjectsMenu = function() {
    this.state = "objectsPicker";
    destroyWindow();
    this.objects.forEach(function(item) {
      addButton(item.name, item.name + "_button");
    });
    addButton("Add object", "addObject");
    addButton("<", "mainMenu");
  };

  this.showTexturesMenu = function() {
    destroyWindow();
    this.textures.forEach(function(item) {
      addButton(item, item);
    });
    addButton("Add texture", "addTexture");
    addButton("<", "graphics");
  };

  this.showGraphicsMenu = function() {
    destroyWindow();
    addButton("Models", "models");
    addButton("Textures", "textures");
    addButton("<", "mainMenu");
  };

  this.addModelAtPath = function(modelPath) {
    var result = copyFileIntoCurrentPath(modelPath);
    if (result == false) { return; }
    var newModelPath = modelPath.lastPathComponent();
    this.models.push(newModelPath);
  };

  this.addTextureAtPath = function(modelPath) {
    var result = copyFileIntoCurrentPath(modelPath);
    if (result == false) { return; }
    var newTexturePath = modelPath.lastPathComponent();
    this.textures.push(newTexturePath);
  };

  this.showModelsMenu = function() {
    destroyWindow();
    this.models.forEach(function(item) {
      addButton(item, item);
    });
    addButton("Add model", "addModel");
    addButton("<", "graphics");
  };

  this.addModel = function() {
    var modelPath = selectFile();
    this.addModelAtPath(modelPath);
    this.showModelsMenu();
  };

  this.addTexture = function() {
    var texturePath = selectFile();
    this.addTextureAtPath(texturePath);
    this.showTexturesMenu();
  };

  this.addObjectMenu = function() {
    destroyWindow();
    if (this.models.length > 0) {
      addText("Select model:");
      this.models.forEach(function(item) {
        addButton(item,item);
      });
    }
    else {
      addText("Add models first!");
    }
    addButton("<", "objects");
  };

  this.addObjectInFrontOfCameraWithModelPath = function(modelPath) {
    var objectName = uuid();
    var object = createObject();
    object.name = objectName;
    var camera = getObject("camera");
    object.rotation = newVectorFromCopy(camera.rotation);
    object.position = newVectorFromCopy(camera.position);
    object.modelPath = modelPath;
    translateCamera(object, 0, 0, 2);
    addObject(object);
    var sceneObject = new SceneObject(objectName, modelPath);
    this.objects.push(sceneObject);
    this.showObjectsMenu();
  };

  this.step = function() {
    if (isKeyPressed("jumpKey") && this.state == "freeFly") {
      disablePointerLock();
      this.showMainMenu();
    }
    if (this.state == "default") {
      this.defaultStateStep();
    }
    else if (this.state == "addObjectModelPicker") {
      this.addObjectModelPickerStep();
    }
    else if (this.state == "objectsPicker") {
      this.objectsPickerStep();
    }
    else if (this.state == "freeFly") {
      destroyWindow();
      addText("Press [SPACE] to exit Free Fly mode");
      enablePointerLock();
      this.freeViewManager.step();
    }
    else if (this.state == "objectControl") {
      this.objectControlMenuStep();
    }
  };

  this.showObjectControlMenu = function(objectName) {
    this.state = "objectControl";
    this.selectedObjectName = objectName;
    destroyWindow();
    addButton("Position", "objectPosition");
    addButton("Rotation", "objectRotation");
    addButton("Scale", "objectScale");
    if (this.selectedObjectName != "camera") {
      addButton("Delete", "deleteObject");
    }
    addButton("<", "objects");
  };

  this.vectorFromUserInput = function(rawVector) {
    var splittedVector = rawVector.split(",");
    if (splittedVector.length == 3) {
      var x = Double.parseDouble(splittedVector[0]);
      var y = Double.parseDouble(splittedVector[1]);
      var z = Double.parseDouble(splittedVector[2]);
      var vector = newVector(x,y,z);
      return vector;
    }
    else {
      print("Incorrect vector format");
      return null;
    }
  };

  this.fillVectorFromUserInput = function(object, vector) {
    print(vector.x);
    print(vector.y);
    print(vector.z);
    var rawVector = prompt("x,y,z >");
    if (rawVector.length < 1) {
      print("empty vector, don't apply");
    }
    else {
        var inputVector = this.vectorFromUserInput(rawVector);
        if (inputVector != null) {
          vector.x = inputVector.x;
          vector.y = inputVector.y;
          vector.z = inputVector.z;
          updateObject(object);
        }
    }
  };

  this.objectControlMenuStep = function() {
    print("get object: " + this.selectedObjectName);
    var object = getObject(this.selectedObjectName);
    if (isButtonPressed("objectPosition")) {
      this.fillVectorFromUserInput(object, object.position);
    }
    else if (isButtonPressed("objectRotation")) {
      this.fillVectorFromUserInput(object, object.rotation);
    }
    else if (isButtonPressed("objectScale")) {
      this.fillVectorFromUserInput(object, object.scale);
    }
    else if (isButtonPressed("objects")) {
      this.showObjectsMenu();
    }
    else if (isButtonPressed("deleteObject")) {
      removeObject(this.selectedObjectName);
      var self = this;
      this.objects.removeMatchAll(function(item) {
          return item.name == self.selectedObjectName;
      });
      this.showObjectsMenu();
    }
  };

  this.objectsPickerStep = function() {
    var self = this;
    this.objects.forEach(function(item) {
      if (isButtonPressed(item.name + "_button")) {
        self.showObjectControlMenu(item.name);
      }
    });
    if (isButtonPressed("addObject")) {
      this.state = "addObjectModelPicker";
      this.addObjectMenu();
    }
    else if (isButtonPressed("mainMenu")) {
      this.showMainMenu();
    };
  };

  this.addObjectModelPickerStep = function() {
    if (isButtonPressed("objects")) {
      this.showObjectsMenu();
    }
    else {
      var self = this;
      this.models.forEach(function(item) {
        if (isButtonPressed(item)) {
          self.addObjectInFrontOfCameraWithModelPath(item);
        }
      });
    }
  };

  this.saveScene = function() {
    var scene = new Object();
    scene.objects = new List();
    this.objects.forEach(function(item) {
      var object = getObject(item.name);
      object.modelPath = item.modelPath;
      scene.objects.push(object);
    });
    GLOBAL_CONTEXT.scene = scene;
    GLOBAL_CONTEXT.models = this.models;
    GLOBAL_CONTEXT.textures = this.textures;
    GLOBAL_CONTEXT.scripts = this.scripts;
    GLOBAL_CONTEXT.music = this.music;
    GLOBAL_CONTEXT.sound = this.sound;
    saveVariable("GLOBAL_CONTEXT");
  };

  this.loadScene = function() {
    removeAllObjects();
    include("save.js");
    var list = new List();
    list.addTraits(GLOBAL_CONTEXT.scene.objects);
    list.addTraits(GLOBAL_CONTEXT.models);
    list.addTraits(GLOBAL_CONTEXT.textures);
    list.addTraits(GLOBAL_CONTEXT.scripts);
    list.addTraits(GLOBAL_CONTEXT.music);
    list.addTraits(GLOBAL_CONTEXT.sound);
    addDefaultCamera();
    GLOBAL_CONTEXT.scene.objects.forEach(function(item) {
      if (item.name == "camera") {
        var camera = getObject("camera");
        camera.position = item.position;
        camera.rotation = item.rotation;
        camera.scale = item.scale;
        updateObject(camera);
      }
      else {
        var object = createObject();
        object.name = item.name;
        object.modelPath = item.modelPath;
        object.position = item.position;
        object.rotation = item.rotation;
        object.scale = item.scale;
        addObject(object);
      }
    });

    this.models = GLOBAL_CONTEXT.models;
    this.textures = GLOBAL_CONTEXT.textures;
    this.scripts = GLOBAL_CONTEXT.scripts;
    this.music = GLOBAL_CONTEXT.music;
    this.sound = GLOBAL_CONTEXT.sound;

    this.showMainMenu();
  };

  this.defaultStateStep = function() {
    if (isButtonPressed("objects")) {
      this.showObjectsMenu();
    }
    else if (isButtonPressed("graphics")) {
      this.showGraphicsMenu();
    }
    else if (isButtonPressed("mainMenu")) {
      this.showMainMenu();
    }
    else if (isButtonPressed("models")) {
      this.showModelsMenu();
    }
    else if (isButtonPressed("textures")) {
      this.showTexturesMenu();
    }
    else if (isButtonPressed("addModel")) {
      this.addModel();
    }
    else if (isButtonPressed("addTexture")) {
      this.addTexture();
    }
    else if (isButtonPressed("freeFly")) {
      this.state = "freeFly";
    }
    else if (isButtonPressed("save")) {
      this.saveScene();
    }
    else if (isButtonPressed("load")) {
      this.loadScene();
    }
  };
};
