function MainController() {
  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      addDefaultCamera();
      var extensions = new IOExtensions();
      extensions.enable();
      this.menuController = new MenuController();
      this.menuController.initializeIfNeeded();
      this.initialized = true;
    }
  };

  this.step = function() {
    this.initializeIfNeeded();
    this.menuController.step();
  };
};
