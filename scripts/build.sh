python3 project/resourcesSourcesAndTools/tools/compileIncludes.py
rm -rf tools/flame-steel-chaos/build
mkdir tools/flame-steel-chaos/build
scripts/buildScripts/linux/build.sh
cp build/linux/FlameSteelEngineProject/FlameSteelEngineProject tools/flame-steel-chaos/build
cp tools/flame-steel-chaos/project/src/*.js tools/flame-steel-chaos/build
cp project/include/FlameSteelFramework/flame-steel-engine/project/resources/* tools/flame-steel-chaos/build
cp project/include/FlameSteelFramework/flame-steel-engine/project/src/* tools/flame-steel-chaos/build
